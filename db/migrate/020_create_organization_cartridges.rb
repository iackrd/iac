class CreateOrganizationCartridges < ActiveRecord::Migration
  def change
    create_table :organization_cartridges do |t|
      t.belongs_to :organization, index: true, foreign_key: true
      t.integer :cartridge_category_id, index: true
      t.string :name
      t.string :printers
      t.integer :category
    end
    add_index :organization_cartridges, [:organization_id, :cartridge_category_id], unique: true, name: 'index_issue_crtr_on_org_id_and_crtr_category_id'
  end
end
