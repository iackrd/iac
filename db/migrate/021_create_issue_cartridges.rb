class CreateIssueCartridges < ActiveRecord::Migration
  def change
    create_table :issue_cartridges do |t|
      t.integer :issue_id
      t.integer :cartridge_id
      t.string  :name
      t.integer :category
      t.integer :num
      t.integer :filled
      t.integer :restored
      t.integer :killed
      t.integer :newed
    end
    add_index :issue_cartridges, :issue_id
  end
end
