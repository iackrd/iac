class CreateCartridgeCategories < ActiveRecord::Migration
  def up
    create_table :cartridge_categories do |t|
      t.string :name
      t.string :type
    end
    PrinterCategory.distinct.select(:cartridge).each do |c|
      cc=CartridgeCategory.new
      cc.name = c.cartridge
      cc.save
    end
  end

  def down
    drop_table :cartridge_categories
  end
end
