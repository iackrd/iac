# frozen_string_literal: true

class IacHook < Redmine::Hook::ViewListener
  render_on :view_issues_show_description_bottom, partial: 'issues/show_description_bottom'
  render_on :view_issues_form_details_top,        partial: 'issues/form/details/top'
  render_on :view_issues_form_details_bottom,     partial: 'issues/form_details_bottom'
  render_on :view_projects_show_right,            partial: 'project/list_of_so'

  render_on :view_issues_sidebar_queries_bottom,  partial: 'news/side_bar'
  #  render_on :view_issues_form_details_bottom,     :partial => "issue/author_as_checker"
end
