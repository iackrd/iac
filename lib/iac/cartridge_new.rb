module Iac
  module CartridgeNew

    STATUS_NEW           = 1  # Новая
    STATUS_FORMED        = 29 # Сформирована
    STATUS_TAKEN         = 30 # Принята
    STATUS_WORK          = 2  # В работе
    STATUS_PASSED        = 28 # На выдачу
    STATUS_CLOSE         = 5  # Закрыто


    def cartridge_updete context
      issue = context[:issue]
      current_status = get_status_id Issue.find(issue.id).status.name #проверяем текущий статус
      new_status     = get_status_id issue.status.name

      # begin Раздел обработки Общих задач
      if issue.project.common? &&
          ((current_status == 4 && new_status == 5) ||
              (current_status == 5 && new_status == 4))
        # tracker.name == 'Картриджи'
        # project.identifier == 'all'
        # Текущий статус задачи "В работе"
        #Устанавливаем признак по которому потом поймем что надо закрывать подчиненные задачи0
        context[:params].merge!({update_descendants_status: true})
      end

      #Раздел обработки задач Объектов автоматизации
      if !issue.project.common? &&
         (cartridges = context[:params][:cartridges])
        case new_status
          when 1,2
            issue.issue_cartridges.clear
            cartridges.each do |cartridge_id, value|
              organization_cartridge = OrganizationCartridge.find_by(cartridge_category_id: cartridge_id)
              unless (num = value[:num].to_i) == 0
                issue.issue_cartridges.build(cartridge_id: cartridge_id, name: organization_cartridge.name, category: organization_cartridge.category, num: num)
              end
            end
          when 5
            cartridges.each do |cartridge_id, value|
              cartridge = issue.issue_cartridges.find_by(cartridge_id: cartridge_id)
              num = cartridge.num
              cartridge.restored = restored = value[:restored].to_i
              cartridge.killed   = killed   = value[:killed].to_i
              cartridge.filled   = num - restored - killed
              cartridge.save
            end
        end
      end
    end

    private

    def common_cartridges_filled
      @issue.cartridges_by_type.each do |item|
        tmp_s = item.send_count
        tmp_r = @restored[item.cartridge]
        tmp_k = @killed[item.cartridge]
        if tmp_s >= (tmp_r + tmp_k)
          r_part = tmp_r/tmp_s
          k_part = tmp_k/tmp_s
          @issue.cartridges.joins(printer: :printer_category)
              .where(printer_categories: {cartridge: item.cartridge})
              .order('send').all.each do |pc|
            pc_r = (pc.send * r_part).ceil
            pc_r = tmp_r if tmp_r < pc_r
            tmp_r = tmp_r - pc_r

            pc_k = (pc.send * k_part).ceil
            pc_k = tmp_k if tmp_k < pc_k
            tmp_k = tmp_k - pc_k

            pc.filled   = pc.send - (pc_r + pc_k)
            pc.restored = pc_r
            pc.killed   = pc_k
            pc.save
          end
        else
          @error = true
        end
      end
    end

    def get_status_id status_name
      case status_name
        when 'Новая'        then  1
        when 'Сформирована' then  2
        when 'Принята'      then  3
        when 'В работе'     then  4
        when 'На выдачу'    then  5
        when 'Закрыта'      then  6
        else                      nil
      end
    end

    def get_status_name id
      case id
        when 1 then 'Новая'
        when 2 then 'Сформирована'
        when 3 then 'Принята'
        when 4 then 'В работе'
        when 5 then 'На выдачу'
        when 6 then 'Закрыта'
        else        nil
      end
    end
  end
end