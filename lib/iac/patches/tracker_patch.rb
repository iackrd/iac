#!/bin/env ruby
# encoding: utf-8

module Iac
  module Patches
    module TrackerPatch
      def self.included(base) # :nodoc:
        base.extend(ClassMethods)
        base.send(:include, InstanceMethods)
        base.class_eval do
          unloadable # Send unloadable so it will not be unloaded in development
          # has_and_belongs_to_many :contacts, :uniq => true

        end
      end

      module  ClassMethods
        def new_cartridge_id
          Setting.plugin_iac['tracker_new_cartridge']
        end
      end

      module InstanceMethods
        def category
           case self.id
             when Setting.plugin_iac['tracker_cartridge'].to_i
               :tracker_cartridge
             when Setting.plugin_iac['tracker_new_cartridge'].to_i
               :tracker_new_cartridge
             when Setting.plugin_iac['tracker_recovery'].to_i
               :tracker_recovery
             when Setting.plugin_iac['tracker_administration'].to_i
               :tracker_administration
             when Setting.plugin_iac['tracker_unscheduled'].to_i
               :tracker_unscheduled
             when Setting.plugin_iac['tracker_planing'].to_i
               :tracker_planing
             when Setting.plugin_iac['tracker_repair'].to_i
               :tracker_repair
           end
        end
      end
    end
  end
end

unless Tracker.included_modules.include?(Iac::Patches::TrackerPatch)
  Tracker.send(:include, Iac::Patches::TrackerPatch)
end