#!/bin/env ruby
# encoding: utf-8
require_dependency 'issue'
require_dependency 'issue_cartridge'
require_dependency 'equipment_request'
require_dependency 'equipment'
require_dependency 'issue_repairs'

IssueQuery.available_columns << QueryColumn.new(:equipment_category)
IssueQuery.available_columns << QueryColumn.new(:repair_num)
IssueQuery.available_columns << QueryColumn.new(:invnumber)
IssueQuery.available_columns << QueryColumn.new(:sernumber)

module Iac
  module Patches
    module IssuePatch
      def self.included(base) # :nodoc:
        base.extend(ClassMethods)
        base.send(:include, InstanceMethods)
        base.class_eval do
          unloadable # Send unloadable so it will not be unloaded in development
          #default_scope {preload(equipment_request: [equipment: :equipment_category])}
          def self.default_scope
            # В списке задач - реально ускоряет вывод ремонтов, а вот при выводе связанных задач по ремонтам,
            # вызывается для каждой задачи и реально тормозит
            # preload(equipment_request: [:service_request, equipment: :equipment_category])
          end
          # has_and_belongs_to_many :contacts, :uniq => true
          delegate :oa_id_iac, :organization, :common?, to: :project

          has_one :equipment_request, autosave: true, inverse_of: :issue, dependent: :destroy
          delegate :service_request, to: :equipment_request
          delegate :oa_name, :oa_code, to: :project

          has_many :issue_cartridges, dependent: :delete_all

          safe_attributes 'service_request'

          validate :due_date_in_future
          validate :repair_validate
          after_save :repair_after_save

          protected
          def due_date_in_future
            return true if due_date.nil?
            if due_date.to_time > Date.today.beginning_of_day+30.days
              errors.add :due_date, :not_in_future
            end
          end
        end
      end


      module  ClassMethods
        @@mult_issue = []
        def multiply(issue_id)
          @cur_issue = Issue.find(issue_id)
          #TODO Блок проверяющий не выполняется ли аналогичная задача в другом инстансе

          flash = []
          flash << 'Задача уже размножается' if @@mult_issue.include?(@cur_issue.id)
          flash << 'не укзаны трекер и/или Вид работ для дочерних задач' unless ( @child_tracker = Tracker.find_by_name(@cur_issue.cf_by_name('Вид работ'))) or @cur_issue.cf_by_name('Вид работ') != ''

          if flash.empty?

            @@mult_issue = @@mult_issue + [@cur_issue.id]
            @cast_field_code_oa = CustomField.find_by_name('Код ОА')
            @cast_field_request = CustomField.find_by_name('Текст заявки')
            @cast_field_checker = CustomField.find_by_name('Проверяющий')
            @cast_field_number  = CustomField.find_by_name('Номер производства')

            Organization.rs.all.each do |org|
              project = org.project
              #Если вложенная задача существует переходим к следующему суду
              next if (Issue.where(:parent_id => @cur_issue.id, :project_id => project.id).count > 0)
              #Вложенной задачи нет, поэтому создаем новую
              child_issue = Issue.new
              child_issue.project         = project
              child_issue.assigned_to_id  = project.cf_by_name('Ответственный инженер')
              child_issue.subject         = @cur_issue.subject
              child_issue.author          = @cur_issue.assigned_to
              child_issue.parent_issue_id = @cur_issue.id
              child_issue.start_date      = @cur_issue.start_date
              child_issue.tracker         = @child_tracker
              child_issue.status_id       = 1 #Новая
              child_issue.custom_field_values = {@cast_field_request.id.to_s => @cur_issue.custom_value_for(@cast_field_request).value,
                                                 @cast_field_checker.id.to_s => @cur_issue.assigned_to.id.to_s
              }
              child_issue.save!
            end

            @@mult_issue = @@mult_issue - [@cur_issue.id]
            flash << 'Размножение проведено успешно'
          end
          flash
        end
      end

      module InstanceMethods
        TRACKER_NAME = {3 => 'Восстановление работоспособности',
                        6 => 'Ремонт',
                        11 => 'Настройка и администрирование',
                        12 => 'Внеплановое обновление'}

        include Repairs
        include Iac::IssueRepairs



        def custom_field_by_name(custom_field_name)
          custom_field_id = CustomField.find_by_name(custom_field_name).id
          custom_field_values.select{|item| item.custom_field_id == custom_field_id}.shift
        end

        def cf_by_name(custom_field_name) custom_field_by_name(custom_field_name).to_s; end
        def cstp() custom_field_by_name('ЦCТП').to_s; end

        def zdate
          t = (due_date-1)
          t.strftime("%d.%m.%Y").to_s
        end

        def ztext() custom_field_by_name("Текст заявки").to_s; end
        def inspector() custom_field_by_name('Проверяющий'); end

        def inspector= (c)
          self.inspector.value = c
          #self.custom_field_values = {CustomField.find_by_name('Проверяющий').id.to_s => c}
          self.author_id = c
        end

        def issue_date
          t=due_date
          t.strftime("%d.%m.%Y").to_s
        end

        def full_tracker_name
          TRACKER_NAME[tracker_id]
        end

        # Суммы всех картриджей в задаче
        def count_cartridges
          if common?
            ids = []
            descendants.each {|i| ids<<i.id}
          else
            ids = id
          end
          PrintersIssue
              .select('sum(printers_issues.crtr_plan) as plan_count,'+
                          ' sum(printers_issues.crtr_real)     as real_count,'+
                          ' sum(printers_issues.crtr_send)     as send_count,'+
                          ' sum(printers_issues.crtr_filled)   as filled_count,'+
                          ' sum(printers_issues.crtr_restored) as restored_count,'+
                          ' sum(printers_issues.crtr_killed)   as killed_count')
              .joins(printer: :printer_category)
              .where(printers_issues: {issue_id: ids})
              .first
        end

        # Суммы картриджей в задаче по типам
        def cartridges_by_type
          if common?
            ids = []
            descendants.each {|i| ids<<i.id}
          else
            ids = id
          end
          PrintersIssue
            .select('printer_categories.cartridge as cartridge,'+
                        'printer_categories.brand as c_brand,'+
                        ' sum(printers_issues.crtr_plan)     as plan_count,'+
                        ' sum(printers_issues.crtr_real)     as real_count,'+
                        ' sum(printers_issues.crtr_send)     as send_count,'+
                        ' sum(printers_issues.crtr_filled)   as filled_count,'+
                        ' sum(printers_issues.crtr_restored) as restored_count,'+
                        ' sum(printers_issues.crtr_killed)   as killed_count')
            .joins(printer: :printer_category)
            .where(printers_issues: {issue_id: ids})
            .group('c_brand, cartridge')
        end

        def tracker_for_user?(tracker_id)
          RoleTrackers.where(tracker_id: tracker_id, role_id: User.current.roles_for_project(project)).exists?
        end

        def status_cartridge_order
          case status.name
            when 'Новая'        then  1
            when 'Сформирована' then  2
            when 'Принята'      then  3
            when 'В работе'     then  4
            when 'На выдачу'    then  5
            when 'Закрыта'      then  6
            else                      nil
          end if [:tracker_new_cartridge, :tracker_cartridge].include? tracker.category
        end
        
        # До 2017 года
        def cartridges
          ids = common? ? descendants.ids : id
          PrintersIssue.where(issue_id: ids)
        end

        #С 2017 года
        def cartridge(category_id)
          self.issue_cartridges.where(cartridge_id: category_id).first
        end

        def issue_cartridges_descendants
          ids = common? ? descendants.ids : id
          IssueCartridge
              .select(' cartridge_id, name, category, '+
                          ' sum(num)      as num,'+
                          ' sum(filled)   as filled,'+
                          ' sum(restored) as restored,'+
                          ' sum(killed)   as killed')
              .where(issue_id: ids)
              .order('category, name')
              .group('cartridge_id, name, category')
        end

        # Суммы картриджей в задаче по типам
        def cartridges_new_by_type
          ids = common? ? descendants.ids : id
          IssueCartridge
              .select(' category, '+
                      ' sum(num)      as num,'+
                      ' sum(filled)   as filled,'+
                      ' sum(restored) as restored,'+
                      ' sum(killed)   as killed')
              .where(issue_id: ids)
              .order('category')
              .group('category')
        end

        # Суммы всех картриджей в задаче
        def count_cartridges_new
          ids = common? ? descendants.ids : id
          IssueCartridge
              .select('sum(num)      as num,'+
                     ' sum(filled)   as filled,'+
                     ' sum(restored) as restored,'+
                     ' sum(killed)   as killed')
              .where(issue_id: ids)
              .first
        end
      end
    end
  end
end

unless Issue.included_modules.include?(Iac::Patches::IssuePatch)
  Issue.send(:include, Iac::Patches::IssuePatch)
end



