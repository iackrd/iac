#!/bin/env ruby
# encoding: utf-8
require_dependency 'issue'
require_dependency 'organization'

module Iac
  module Patches
    module ProjectPatch
      def self.included(base) # :nodoc:
        base.extend(ClassMethods)
        base.send(:include, InstanceMethods)
        base.class_eval do
          unloadable # Send unloadable so it will not be unloaded in development
          # has_and_belongs_to_many :contacts, :uniq => true
          has_one :organization
          delegate :oa_id_iac, :places_for_select, to: :organization

          scope :new_for_tp, ->(){
            includes(:organization).
                where(:parent_id => Setting.plugin_iac[:main_tp_project]).
                all.select{|i| !i.organization} if Setting.plugin_iac[:main_tp_project]
          }
        end
      end

      module  ClassMethods
        def common_id
          @common_id ||= Setting.plugin_iac['common_tp_project'].to_i
        end
      end

      module InstanceMethods

        def common?
          id == Project.common_id
        end

        def cf_by_name fname
          @@cf ||= {}
          @@cf[id] ||= {}
          @@cf[id][fname] ||= custom_value_for(CustomField.find_by_name fname).to_s
        end

        def oa_name
          cf_by_name("Наименование ОА")
        end

        def oa_code
          cf_by_name("Код ОА")
        end

        def oa_user
          cf_by_name('Ответственный инженер')
        end

        def system_code
          case cf_by_name("Код ОА")[2..3]
            when "RS"
              "081"
            when "GV"
              "083"
            when "UD"
              "079"
            when "OS"
              "080"
          end
        end

        def engineer
          if (project_cf_engineer = self.cf_by_name('Ответственный инженер'))&&!project_cf_engineer.blank?
            User.find(project_cf_engineer)
          end
        end

        def loanee_fio
          case project.identifier
            when 'kks' then "С.Н. Свашенко"
            when 'as32' then "А.Е. Егоров"
            when 'ao08' then "А.Д. Шишкин"
            else "И.И. Гарбовский"
          end
        end

        def loanee_full_name
          case project.identifier
            when 'kks' then "Свашенко Сергей Николаевич"
            when 'as32' then "Егоров Алексей Евгеньевич"
            when 'ao08' then "Шишкин Алексей Дмитриевич"
            else "Гарбовский Иосиф Иванович"
          end
        end

        def loanee_basis
          case project.identifier
            when 'kks' then 'федерального закона от 31.12.1996 г. № 1-ФКЗ "О Судебной системе Российской Федерации"'
            when 'as32' then 'федерального закона от 28.04.1995 г. № 1-ФКЗ "Об арбитражных судах в Российской Федерации"'
            when 'ao08' then 'федерального закона от 28.04.1995 г. № 1-ФКЗ "Об арбитражных судах в Российской Федерации"'
            else 'федерального закона от 08.01.1998 г. № 7-ФЗ "О Судебном департаменте при Верховном Суде Российской Федерации" и Положения об Управлении Судебного департамента в Краснодарском крае'
          end
        end

        def loanee_pos
          case project.identifier
            when 'kks' then "Заместитель председателя Краснодарского краевого суда"
            when 'as32' then "председатель Арбитражного суда Краснодарского края "
            when 'ao08' then "председатель Арбитражного суда Северо-Кавказского округа"
            else "Начальник Управления судебного департамента в Краснодарском крае"
          end
        end

      end
    end
  end
end

unless Project.included_modules.include?(Iac::Patches::ProjectPatch)
  Project.send(:include, Iac::Patches::ProjectPatch)
end



