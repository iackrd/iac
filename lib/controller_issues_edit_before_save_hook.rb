#!/bin/env ruby
# encoding: utf-8
# require_dependency 'iac/'
module Iac
  class ControllerIssuesEditBeforeSaveHook < Redmine::Hook::ViewListener
    include Repairs
    # include Cartridges
    include Iac::CartridgeNew

    def controller_issues_edit_before_save(context={})

      #Блок Копирования проверяющего в автора для реализации
      context[:issue].author_id = context[:issue].inspector.value if context[:issue].inspector && !context[:issue].inspector.blank?

      case context[:issue].tracker.category
        # when :tracker_cartridge #Раздел обработки задач с трекером 'Картриджи'
        #   cartridge_updete context
        when :tracker_new_cartridge #Раздел обработки задач с трекером 'Картриджи'
          cartridge_updete context
        when :tracker_repair
          repair_update(context) if context[:issue].can_repair_start_edit?()
      end
    end
  end
end