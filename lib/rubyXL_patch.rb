#!/bin/env ruby
# encoding: utf-8
class RubyXL::Workbook
  def self.make(template, params)
    template_workbook = RubyXL::Parser.parse(template)
    out_workbook      = RubyXL::Parser.parse(template)
    template_worksheet = template_workbook.worksheets[0]
    # out_workbook = RubyXL::Workbook.new
    out_worksheet = out_workbook.worksheets[0]
    out_worksheet.sheet_data.rows = []
    # out_worksheet.sheet_name = template_worksheet.sheet_name
    out_worksheet.replace_block(template_worksheet.sheet_data.rows, params)

    # template_workbook.worksheets.each do |template_worksheet|
    #   out_worksheet = out_workbook.add_worksheet(template_worksheet.sheet_name)
    #   out_worksheet.replace_block(template_worksheet.sheet_data.rows, params)
    # end
   out_workbook
  end
end

class RubyXL::Worksheet

  # Расширяем класс методом копирования строки из другого Workbook
  def append_row(row)
    @row_index ||= 0
    validate_workbook
    new_row = new_cells = nil
    sheet_data.rows.insert(@row_index, nil)

    if row then
      new_cells = row.cells.collect do |c|
        if c.nil? then
          nil
        else
          ncv = RubyXL::CellValue.new(value: c.value)
          nc = RubyXL::Cell.new(
              :style_index => c.style_index,
              value_container: ncv
          )
          nc.worksheet = self
          nc.row = @row_index
          nc.column = c.column
          nc
        end
      end
      new_cells ||= Array.new(0)
      new_row = add_row(@row_index, :cells => new_cells, :style_index => row&&row.style_index)
      new_row.r = @row_index
      new_row.ht = row.ht
    end

    @row_index += 1
    new_row
  end


  def replace_block (rows, params)
    cycle = false
    rows.each_with_index do |row, num_row|
      unless cycle
        if row&&row.cells[0]&&/<%start\((\w+)\)%>/.match(row.cells[0].value.to_s)
          cycle = true
          @objects_name = $1.to_sym
          @start_row_num = num_row
        else
          new_row = append_row(row)
          replace_row(new_row, params)
        end
      else
        if row&&row.cells[0]&&/<%end\(#{@objects_name}\)%>/.match(row.cells[0].value.to_s)
          end_row_num = num_row
          sub_rows = rows[(@start_row_num+1)..(end_row_num-1)]
          objects =
              unless params["#{@objects_name}_slave"]
                params[@objects_name]
              else
                master_object_name = params["#{@objects_name}_master_name"]
                params[@objects_name].call(params[master_object_name])
              end
          objects.each do |object|
            sub_params = params.merge @objects_name.to_s.singularize.to_sym => object
            self.replace_block(sub_rows, sub_params)
          end if objects
          cycle = false
        end
      end
    end
  end

  def replace_row (row, params)
    return if row.nil?
    row.cells.each do |cell|
      next if cell.nil?
      cht = replace_text(cell.value.to_s, params)
      cell.change_contents(cht)
    end
  end

  def replace_text (text, params)
    t=String.new(text)
    t.gsub!(/<%(\w+)%>/)          { params[$1.to_sym].to_s }
    t.gsub!(/<%(\w+)\.(\w+)%>/)   { params[$1.to_sym].send($2).to_s }
    t.gsub!(/<%(\w+)\[(\w+)\]%>/) { params[$1.to_sym].call(params[$2.to_sym])
    }
    t
  end
end
