/**
 * Created by ruby on 07.05.15.
 */

function ff(t,a)
{
    var namecard = t.name.replace(/cartridges\[([^\]]*)\]\[(restored|killed)\]/g,"$1");
    var num       =+$("#num_"     +namecard)[0].innerHTML;
    var $restored = $("#cartridges_"+namecard+"_restored")[0];
    var $killed   = $("#cartridges_"+namecard+"_killed")[0];
    var $filled   = $("#cartridges_"+namecard+"_filled")[0];

    if ($restored.value > $restored.max) $restored.value = $restored.max;
    if ($killed.value   > $killed.max)   $killed.value   = $killed.max;
    if(a==0) $killed.max   = num - $restored.value;
    else     $restored.max = num - $killed.value;

    $filled.value = num - $restored.value - $killed.value;
}