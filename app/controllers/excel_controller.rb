require 'rubyXL'
require 'rubyXL_patch'

class ExcelController < ApplicationController
  PORUCHENIYA   = ['15.05.2014','20.02.2015','09.04.2019']
  TEMPLATESPATH = Dir.pwd + '/plugins/iac/templates/'
  protect_from_forgery except: :make_act_co3

  TRACKERS = [
    Setting.plugin_iac['tracker_recovery'],
    Setting.plugin_iac['tracker_administration'],
    Setting.plugin_iac['tracker_unscheduled']
  ]

  def make_co3
    template  = TEMPLATESPATH + "report_so3.xlsx"
    year = params[:year].to_i
    num =  params[:num].to_i
    from = "01.01.#{year}".to_date
    to   = (num==4 ? "01.01.#{year+1}" : "01.#{num*3 + 1}.#{year}").to_date


    organisations = Organization.where(organization_category_id: [6,7,10]).order(:organization_category_id, :code).all
    project_ids = Array.wrap(organisations).map(&:project_id)

    query = Issue.where(project_id: project_ids, tracker_id: TRACKERS, due_date: from..to)
    issues = query.select("project_id, tracker_id, count(*) as c ").group("project_id, tracker_id").all
    project_issues_count ||= {}
    total_issues = 0
    issues.each do |issue|
      total_issues ||= 0
      project_issues_count.merge!(issue.project_id => {count: 0, trackers: {}}) unless project_issues_count[issue.project_id]
      project_issues_count[issue.project_id][:trackers].merge!(issue.tracker_id => {name: issue.tracker.name, count: issue.c})
      project_issues_count[issue.project_id][:count] += issue.c
      total_issues += issue.c
    end

    cf_id = CustomField.where(name: "С выездом на ОА").first.id
    outputs_count = query.joins(:custom_values).where(custom_values: {custom_field_id: cf_id, value: "1"}).count

    params =
        {
            num:           num,
            year:          year,
            total_acts:    num*3*organisations.count, # В квартале 3 месяца
            total_issues:  total_issues,
            outputs_count: outputs_count,
            organizations: organisations,
            act_dates:     (1..num*3).collect{|month| "01.#{month}.#{year}".to_date.month_last_work_day.strftime('%d.%m.%Y')}.join("\n"),

            act_nums:
                Proc.new do |organization|
                  act_nums=[]
                  (1..num*3).each{|month| act_nums << "ВР-#{organization.code}-#{month}/#{year}"}
                  act_nums.join("\n")
                end,

            statistic:
                Proc.new do |organization|
                  project = project_issues_count[organization.project_id]
                    statistic = ["За период завершено задач: #{project[:count]}, из них:"] +
                      project[:trackers].values.collect{|tracker| "#{tracker[:name]}: #{tracker[:count]}"}
                  statistic.join("\n")
                end
          }

    workbook = RubyXL::Workbook.make(template, params)
    send_data workbook.stream.string,
              :filename => "otchet.xlsx",
              :type => 'application/msexcel',
              :disposition => 'attachment'
  end

end