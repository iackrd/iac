class CartridgesController < ApplicationController
  unloadable
  before_action { authorize(params[:controller], params[:action], true) } #Проверку общих прав надо делать обязательно до установки переменной @project
  before_action :set_issue_project , only: [ :edit, :update, :receive_to_tank]
  before_action :set_project , only: [ :new_tanker ]

  include Cartridges

  def edit
    #if true
    if @issue.common?
      # && User.current.allowed_to?(:cartridge_process_application, @project)
      @cartridge_category = params[:cartridge]
      @cartridges = @issue.cartridges
      @cartridges = @cartridges.joins(printer: :printer_category).where(printer_categories: {cartridge: params[:cartridge]})
    end
    # test = true
  end

  def update
    @restored = params[:restored]
    @killed   = params[:killed]
    @restored.each do |id, value|
      restored = value.to_i
      killed   = @killed[id].to_i
      pc = PrintersIssue.find(id)
      pc.crtr_restored = restored
      pc.crtr_killed   = killed
      pc.crtr_filled   = pc.crtr_send - restored - killed
      pc.save
    end
    redirect_to issue_path(@issue)
  end

  def receive_to_tank
    if @issue.common?
      alert = 'Общую задачу невозможно принять подобным образом'
    elsif @issue.status_id != STATUS_FORMED
      alert = 'Задачу в данном статусе невозможно перевести в статус "Принята"'
    elsif !(parent = Issue.find_by(project_id: Project.common_id, tracker_id: Tracker.new_cartridge_id, status_id: STATUS_NEW))
      alert = 'Отсутствует общая задача в статусе "Новая"'
    else
      #PrintersIssue.find_by_sql("update printers_issues set crtr_real = crtr_plan, crtr_send = crtr_plan where issue_id = #{@issue.id}")
      @issue.status_id  = STATUS_TAKEN
      @issue.start_date = Date.current
      @issue.parent_id  = parent.id
      @issue.save
      notice = 'Картриджи приняты в заправку'
    end
    redirect_to issue_path(@issue), alert: alert, notice: notice
  end

  def new_tanker
    new_status_id = IssueStatus.find_by_name('Новая').id
    if (tracker_id = Tracker.new_cartridge_id)
      # Выбрать только задачу на картриджи в состоянии 'Новая'
      unless (issue = Issue.find_by(tracker_id: tracker_id, project_id: @project.id, status_id: new_status_id))
        # Создать задачу с трекером 'Картриджи' в текущем проекте Назначить ответственного и Проверяющего
        issue = Issue.new({tracker_id: tracker_id, project_id: @project.id, status_id: new_status_id},{:without_protection => true})
        issue.assigned_to_id = Setting.plugin_iac['cartridge_assigned_to']
        issue.subject = 'Заправка картриджей'
        issue.author_id = User.current.id
        issue.save!
      end
      redirect_to edit_issue_path(issue)
    else
      redirect_to :back
    end

  end

  private

  def set_issue_project
    @issue = Issue.find(params[:id])
    @project = @issue.project
  end

  def set_project
    id = params[:project_id] || params[:project]
    @project = Project.find(id)
  end
end
