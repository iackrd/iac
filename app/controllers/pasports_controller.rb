class PasportsController < TemplatesController
  TEMPLATE = "pasport-ts"
  protect_from_forgery except: :make_pasport

  def make_pasport
    # pr      = {}
    project   = Project.find_by_identifier(params[:project])
    equipment = Equipment.where(setplaceid: project.oa_id_iac)
    pr = {}
    @open_structs = []
    equipment.each do |equipment|
      post_year = ''

      if equipment.getdate == nil
      else
        post_year = equipment.getdate.year
      end
      s = ''
      equipment.service_requests.each do |sr|
        if sr.equipment_request
          issue = Issue.find_by_id(sr.equipment_request.issue_id)
          if issue.status_write_off?
            puts issue.id
            if issue.due_date
              s = 'Списание ' + issue.due_date.year.to_s + 'г.'
            else
              s='Не установлена дата завершения задачи '+issue.id.to_s+'('+issue.status.name+')'
            end
          end
        end
      end
      if equipment.sernumber
        sort = equipment.sernumber.capitalize
      else
        sort = ''
      end

      row = {
          name: equipment.stationname,
          # desc:    'Диагностика ТС: ' + equipment.stationname,
          inv:     'Инв. №: ' + equipment.invnumber.to_s,
          ser:     'Сер. №: ' + equipment.sernumber.to_s,
          guarant: format_date(equipment.warrantyenddate),
          post:    'Поставка ' + post_year.to_s + 'г.',
          spis:    s,
          sort:    sort

      }
      @open_structs << OpenStruct.new(row)
    end
    @open_structs = @open_structs.sort_by(&:sort) # сорировка
    common_static_variable_of_project pr, project
    pr.merge! 'open_struct' => @open_structs
    pr.merge! 'tr' => :detail
    generate_rtf_by_template pr, "#{TEMPLATE}.rtf", "#{TEMPLATE}-#{project.oa_code}.rtf"
  end
end