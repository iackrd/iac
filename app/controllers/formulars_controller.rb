class FormularsController < TemplatesController
  TEMPLATE = "formular"
  protect_from_forgery except: :make_formular

  def make_formular
    from    = params[:from]
    to      = params[:to]
    pr      = {}
    project = Project.find_by_identifier(params[:project])
    issues = Issue.
        where(project_id: project, due_date: from..to, tracker_id: [3, 6,11, 12]).
        order("due_date").all
    @open_structs    = []
    issues.each do |issue|
      unless issue.status_id == 1 # если задача не новая

        if issue.tracker_id == 6 # если ремонты
          request   = issue.equipment_request.service_request
          checks    = [request.check1, request.check2].compact.last
          equipment = request.equipment
          row = {
              issue_id: '#'+issue.id.to_s,
              date:    format_date(issue.start_date),
              date_sort:    issue.start_date,
              problem: checks.problemdscrptn,
              desc:    'Диагностика ТС: ' + equipment.stationname,
              inv:     'Инв. №: '+equipment.invnumber.to_s,
              ser:     'Сер. №: '+equipment.sernumber.to_s,
              act:     'Акт №: '+'ТС-'+project.oa_code+'-'+request.regnum.to_s + '/' + request.regyear.to_s + ' от ' + format_date(issue.start_date)
          }
          @open_structs << OpenStruct.new(row)
          if checks.conclusion_name == 'невозможность восстановительного ремонта, вывод из эксплуатации'
            row = {
                issue_id: '#'+issue.id.to_s,
                date:    format_date(issue.due_date),
                date_sort:    issue.due_date,
                problem: checks.problemdscrptn,
                desc:    'Рекомендация на вывод из эксплуатации ТС: ' + equipment.stationname,
                inv:     'Инв. №: '+equipment.invnumber.to_s,
                ser:     'Сер. №: '+equipment.sernumber.to_s,
                act:     'Акт №: '+'ТС-'+project.oa_code+'-'+request.regnum.to_s + '/' + request.regyear.to_s + ' от ' + format_date(issue.due_date)

            }
            @open_structs << OpenStruct.new(row)
          elsif checks.conclusion_name == 'передача в ремонт, ввод в эксплуатацию после ремонта'
            row = {
                issue_id: '#'+issue.id.to_s,
                date:    format_date(issue.due_date),
                date_sort:    issue.due_date,
                problem: '',
                desc:    'Ввод в эксплуатацию ТС: ' + equipment.stationname.to_s,
                inv:     'Инв. №: '+equipment.invnumber.to_s,
                ser:     'Сер. №: '+equipment.sernumber.to_s,
                act:     'Акт №: '+'ТС-'+project.oa_code+'-'+request.regnum.to_s + '/' + request.regyear.to_s + ' от ' + format_date(issue.due_date)

            }
            @open_structs << OpenStruct.new(row)
          end
        else
          row = {
              issue_id: '#'+issue.id.to_s,
              date:    format_date(issue.due_date),
              date_sort:    issue.due_date,
              desc:  issue.description
          }
          @open_structs << OpenStruct.new(row)
        end

      end
    end
    @open_structs=@open_structs.sort_by(&:date_sort) # сорировка по дате
    common_static_variable_of_project pr, project
    pr.merge! 'open_struct' => @open_structs
    pr.merge! 'tr' => :detail
    generate_rtf_by_template pr, "#{TEMPLATE}.rtf", "#{TEMPLATE}-#{from}-#{to}.rtf"
  end
end