#!/bin/env ruby
# encoding: utf-8
class CoController < ApplicationController

  def multiply
    if params[:id]&&(@cur_issue = Issue.find(params[:id]))
      if (@cur_issue.assigned_to == User.current) or (User.current.id == 1)
        flash[:notice] = Issue.multiply(@cur_issue.id).join(', ')
      else
        flash[:notice] = 'У вас недостаточно прав'
      end
    else
      flash[:notice] = 'Во время размножения произошла ошибка'
    end

    redirect_to issue_path(@cur_issue)
  end



end