class RequisitionsController < TemplatesController
  TEMPLATE = "req"
  protect_from_forgery except: :make_requisition

  def make_requisition
    pr     = {}
    issues = Issue.find(params[:id]).children
    # @issue=Issue.find(16406)
    # puts Issue.find(16406).children
    @open_structs = []
    issues.each do |issue|
      if issue.tracker_id == 6 # если ремонты
        request   = issue.equipment_request.service_request
        checks    = [request.check1, request.check2].compact.last
        equipment = request.equipment
        row       = {
            issue_id: issue.id.to_s,
            date:     format_date(issue.start_date),
            problem:  checks.problemdscrptn,
            name:     issue.subject,
            inv:      equipment.invnumber.to_s,
            ser:      equipment.sernumber.to_s
        }
        @open_structs << OpenStruct.new(row)
      end
    end
    # common_static_variable_of_project pr, project
    pr.merge! 'open_struct' => @open_structs
    pr.merge! 'tr' => :detail
    generate_rtf_by_template pr, "#{TEMPLATE}.rtf", "#{TEMPLATE}.rtf"
  end
end