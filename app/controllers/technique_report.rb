class TechniqueReportController < TemplatesController
  TEMPLATE = "technique_report"
  protect_from_forgery except: :technique_report

  def make_requisition
    pr = {}
    issues = Issue.find(params[:id]).children
    # @issue=Issue.find(16406)
    # puts Issue.find(16406).children
    @open_structs = []
    count_issue=issues.count
    issues.each do |issue|
      if issue.tracker_id == 6 # если ремонты
        request = issue.equipment_request.service_request
        date = issue.due_date.year.to_s + 'г.' if issue.due_date
        row = {
            date: date,
            name_oa: project.oa_name,
            akt: 'ТС-' + project.oa_code + '-' + request.regnum.to_s + '/' + request.regyear.to_s,
            count_issue: count_issue
        }
        @open_structs << OpenStruct.new(row)
      end
    end
    # common_static_variable_of_project pr, project
    pr.merge! 'open_struct' => @open_structs
    pr.merge! 'tr' => :detail
    generate_rtf_by_template pr, "#{TEMPLATE}.rtf", "#{TEMPLATE}.rtf"
  end
end