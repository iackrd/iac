class EquipmentSetsController < ApplicationController
  before_action :set_equipment_set, only: [:show, :edit, :update, :destroy]

  # GET /equipment_sets
  def index
    @@est=params["cont_id"]
    @equipment_sets=[]
    many_set = EquipmentTemplate.where(va_code_int: @@est).first.equipment_unit
    if(many_set.count>0)
      many_set.each_with_index do |set, index|
        @equipment_sets[index]= set.equipment_set
      end
    else
      redirect_to equipment_set_templates_url, alert: "У шаблона: #{EquipmentSetTemplate.where(contentid: @@est).first.name} нет комплектов оборудования."
    end
    @oa=Institution.all
  end

  # GET /equipment_sets/1
  def show
    @est=@@est
    @list_equipment=@equipment_set.equipment
  end

  # GET /equipment_sets/new
  def new
    @equipment_set = EquipmentSet.new
    @est=@@est
  end

  # GET /equipment_sets/1/edit
  def edit
    @est=@@est
  end
  def new_equipments

  end

  # POST /equipment_sets
  def create
    @equipment_set = EquipmentSet.new(equipment_set_params)
    @equipment_set.create_set # заполнение полей с шаблона в комплект оборудования
    if @equipment_set.save
      redirect_to new_equipment_unit_path(:set_id => @equipment_set.id, :set_template_id => equipment_set_params[:complectnum]), notice: 'Комплект оборудования создан успешно.'
    else
      render :new
    end
  end

  # PATCH/PUT /equipment_sets/1
  def update
    if @equipment_set.update(equipment_set_params)
      @equipment_set.update_set
      redirect_to @equipment_set, notice: 'Комплект оборудования успешно изменен.'
    else
      render :edit
    end
  end

  # DELETE /equipment_sets/1
  def destroy
    @equipment_set.destroy
    redirect_to equipment_sets_url, notice: 'Equipment set was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_equipment_set
      @equipment_set = EquipmentSet.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def equipment_set_params
            params.require(:equipment_set).permit(:name, :complectnum, :invnumber, :setplaceid, :cost )
    end
end
