class EquipmentUnitsController < ApplicationController
  before_action :set_equipment_unit, only: [:show, :edit, :update, :destroy]

  # GET /equipment_units
  def index
    @equipment_units = EquipmentUnit.all
  end

  # GET /equipment_units/1
  def show
  end

  # GET /equipment_units/new
  def new
    @equipment_unit = EquipmentUnit.new
    @equipment_templates=EquipmentTemplate.where(va_code_int: params[:set_template_id])
  end

  # GET /equipment_units/1/edit
  def edit
  end
  # POST /equipment_units
  def create
    self.new_eqipment
    #@equipment_unit = EquipmentUnit.new(equipment_unit_params)

    if @equipment_unit.save
      redirect_to @equipment_unit, notice: 'Equipment unit was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /equipment_units/1
  def update
    if @equipment_unit.update(equipment_unit_params)
      redirect_to @equipment_unit, notice: 'Equipment unit was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /equipment_units/1
  def destroy
    @equipment_unit.destroy
    redirect_to equipment_units_url, notice: 'Equipment unit was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_equipment_unit
      @equipment_unit = EquipmentUnit.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def equipment_unit_params
      params.require(:equipment_unit).permit(:complectid,:unittypeid)
      #params[:equipment_unit]
      #params.require(:@est).permit(all)


      @est
    end
end
