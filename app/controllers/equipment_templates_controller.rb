class EquipmentTemplatesController < ApplicationController
  before_action :set_equipment_template, only: [:show, :edit, :update, :destroy]

  # GET /equipment_templates
  def index
    @equipment_templates = EquipmentTemplate.all
  end

  # GET /equipment_templates/1
  def show
    if(@equipment_template.addinteger_3==1)
      @leading="Да"
    else
      @leading="Нет"
    end # данная штука нужна для отображения главного оборудования
  end

  # GET /equipment_templates/new
  def new

    @equipment_template = EquipmentTemplate.new
    if(EquipmentTemplate.where(:va_code_int => params[:va_code_int]).count==0)
      @equipment_template.addinteger_3 = 1
    end # если нет в комплекте главного оборудования то новое оборудование становиться главным комплекта
  end

  # GET /equipment_templates/1/edit
  def edit
  end

  # POST /equipment_templates
  def create
    @equipment_template = EquipmentTemplate.new(equipment_template_params)
    @equipment_template.contentid = EquipmentTemplate.next_id # Присваивает следующе значение ключа
    if @equipment_template.save
      redirect_to new_equipment_template_path(
                      va_code_int: @equipment_template.va_code_int, est_name: @equipment_template.equipment_set_template.name), notice: 'Шаблок  оборудования успешно создан.'
    else
      render :new
    end
  end

  # PATCH/PUT /equipment_templates/1
  def update
    if @equipment_template.update(equipment_template_params)
      Equipment.update_templete(@equipment_template)
      redirect_to @equipment_template, notice: 'Шаблон оборудования успешно обновлен.'
    else
      render :edit
    end
  end

  # DELETE /equipment_templates/1
  def destroy
    @equipment_template.destroy
    redirect_to equipment_set_templates_path+"/#{@equipment_template.va_code_int}", notice: 'Шаблон оборудования успешно удален.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_equipment_template
        @equipment_template = EquipmentTemplate.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def equipment_template_params
      params.require(:equipment_template).permit(:name, :addinteger_1,:addinteger_3, :va_code_int) # здесь указываются те переменые которые видит контроллер при передаче с вьюшки
    end
end
