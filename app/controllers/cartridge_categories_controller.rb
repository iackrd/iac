class CartridgeCategoriesController < ApplicationController
  unloadable

  before_filter :set_cartridge_category, only: [:show, :edit, :update, :destroy]

  def index
    @cartridge_categories = CartridgeCategory.ordered
    respond_to do |format|
      format.html
      format.api { @cartridge_categories }
    end
  end

  def show
  end

  def new
    @cartridge_category = CartridgeCategory.new
  end

  def edit
  end

  def create
    @cartridge_category = CartridgeCategory.new
    @cartridge_category.safe_attributes = params[:cartridge_category]
    if @cartridge_category.save
      redirect_to(cartridge_category_path(@cartridge_category), notice: 'Добавлена категория принтера.')
    else
      render(:new)
    end
  end

  def update
    if @cartridge_category.update_attributes(params[:cartridge_category])
      redirect_to(cartridge_category_path(@cartridge_category), notice: 'Изменена категория принтера.')
    else
      render(:edit)
    end
  end

  def destroy
    @cartridge_category.destroy
    redirect_to(cartridge_categories_path, notice: 'Категория принтера удалена.')
  end

  private

  def set_cartridge_category
    @cartridge_category = CartridgeCategory.find(params[:id])
  end
end
