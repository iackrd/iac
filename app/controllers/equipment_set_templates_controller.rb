class EquipmentSetTemplatesController < ApplicationController
  before_action :set_equipment_set_template, only: [:show, :upload, :edit, :update, :destroy]
  @@error_equipment=[]


  # GET /equipment_set_templates
  def index
    @equipment_set_templates = EquipmentSetTemplate.all
  end

  def id_institution_name_for_exsel
    require 'rubyXL'
    require 'rubyXL_patch'
    workbook = RubyXL::Workbook.new
    worksheet=workbook.worksheets[0]
    instituts =Institution.all
    instituts.each_with_index do |institut, num|
      worksheet.add_cell(num,0 , institut.id)
      worksheet.add_cell(num,1, institut.name)
    end
    send_data workbook.stream.string,
              :filename => "суды.xlsx",
              :type => 'application/msexcel',
              :disposition => 'attachment'
  end

  def error_inv_exel
    @@inv_error
    require 'rubyXL_patch'
    workbook = RubyXL::Workbook.new
    worksheet=workbook.worksheets[0]
    worksheet.add_cell(0,0 , "Инвентарный номер")
    @@inv_error.each_with_index do |inv, num|
      worksheet.add_cell(num+1,0 , inv)
    end
    send_data workbook.stream.string,
              :filename => "Инвентарные номера которые уже есть в базе.xlsx",
              :type => 'application/msexcel',
              :disposition => 'attachment'

  end

  # GET /equipment_set_templates/1
  def show
  end

  # GET /equipment_set_templates/new
  def new
    @equipment_set_template = EquipmentSetTemplate.new
  end

  # GET /equipment_set_templates/1/edit
  def edit
  end

# для вьюхи загрузки
  def show_upload
    if(@@error_equipment.count>=1)
      @error_equipment=@@error_equipment
      @@inv_error=@@error_equipment
      @@error_equipment=[]
    end
    if(@error_equipment.nil?)
      @@inv_error=@@error_equipment
    end
    @equipment_set_template =  EquipmentSetTemplate.all.where(contentid: params[:id]).first
  end

  def upload
    @equipment_set_template =  EquipmentSetTemplate.all.where(contentid: params[:id]).first
    if (params[:item_list].nil?)
      redirect_to  equipment_set_template_path(params[:id])+"/upload", alert: 'Файл не загружен, выберите файл и попробуйте снова.'
    else
      require 'csv'
      csv_text =File.read(params[:item_list].path)
      if(csv_text.index("inv")==0) # проверка первым символом должна быть запись inv
        if(csv_text.index("oa")==4) # проверяем чтоб оa был во второй ячейке
          csv_text = csv_text.sub(csv_text.split("\n")[0]+"\n",'') # Убираем первую строчку так как это заголовок
          @array_equipment = CSV.parse(csv_text, :headers => true, :col_sep => ",")# Столбцы разделяются ,
          self.csv_test(@array_equipment)
          self.inv_test(@array_equipment)
          if(@switch !=1)
            @equipment_set_template.create_csv(@array_equipment) # фуункция сохранения шаблона оборудования с csv
            redirect_to equipment_sets_path+"?cont_id=#{@equipment_set_template.contentid}", notice: 'Оборудование успешно добавлено.'
          end
        end
      end
    end
  end

# Проверка файла чтоб удовлетворял  условиям шаблна
  def csv_test(csv)
    oa=Institution.ids
    csv.each_with_index do |set, num|
 #проверяем соответствует ли запись в csv в поле ОА с id ОА в базе
      if (oa.index(set["23"].to_i).nil?)
        redirect_to equipment_set_templates_url+"/"+(params[:id]), alert: "Оборудование не загружено. В файле #{params[:item_list].original_filename}, в столбце OA, в строке #{num+3}(содержащей: #{set["23"]}) допущена ошибка. Замените идентификатор, на идентификатор Объекта автоматизации и попробуйте снова."
        @switch=1
        return
      end
 #проверяем чтоб количество единиц техники в шаблоне комплекта соответствовало количеству техники в комплекте csv файла
      if(set.count-2 != @equipment_set_template.equipment_template.count)
        redirect_to equipment_set_templates_url+"/"+(params[:id]), alert: "Оборудование не загружено. Не соответствует количество оборудования в копмлекте шаблона. Количество оборудования в шаблоне (#{@equipment_set_template.equipment_template.count}), количество оборудования #{params[:item_list].original_filename}(#{set.count-2}).Исправте ошибку и попробуйте снова."
        @switch=1
        return
      end
    end
    equipment_template=EquipmentTemplate.where(va_code_int: @equipment_set_template.contentid)
    row_csv=csv.first
    equipment_template.each do |et|
      unless (row_csv.has_key?"#{et.contentid}")
        redirect_to equipment_set_templates_url+"/"+(params[:id]), alert: "Оборудования с идентификатором #{et.contentid} не найдено в файле #{params[:item_list].original_filename}. Исправте ошибку и попробуйте снова."
        @switch=1
        return
      end
    end

  end

  def inv_test (csv)
    if(@switch !=1)
      equipments = Equipment.all
      @@error_equipment=[]
      csv.each do |row|
        if(equipments.where("invnumber  LIKE '%#{row["22"]}%'").count>0)
          @@error_equipment.push("#{row["22"]}")
          @switch =1
          @ser_num=row[0]
          if(@equipment_set_template.equipment_template.count>1) # Проверка что в комплекте больше одного оборудования
            eq=equipments.where("invnumber  LIKE '%#{row["22"]}%'")# Присваиваем все оборудование с данным инвенарным
            eq.each do |equipment|
              if(equipment.equipment_unit.nil?)# проверяем есть ли привязаность к комплекту
                @switch_unit=1
              else
                @switch_unit=nil
                break
              end
            end
            if(@switch_unit==1)
              EquipmentSet.create_repeated_invnum(row,eq,@equipment_set_template)
            end
          end
        end
      end
      if (@switch ==1)
        if(@equipment_set_template.equipment_template.count==1)
          Equipment.update_csv_error(@ser_num,@@error_equipment, @equipment_set_template)
        end
        redirect_to equipment_set_templates_url+"/"+(params[:id])+"/upload"
        return
      end
    end
  end

# Приводит дату к нужному виду
  def date
    addvarchar_1= params[:addvarchar_1] #.gsub("-",".")+" 00:00"
    @equipment_set_template.addvarchar_1=addvarchar_1
    addvarchar_2= params[:addvarchar_2] #.gsub("-",".")+" 00:00"
    @equipment_set_template.addvarchar_2=addvarchar_2
    if(addvarchar_1<addvarchar_2)
      @equipment_set_template.save
    end
  end


  # POST /equipment_set_templates
  def create
    @equipment_set_template = EquipmentSetTemplate.new(equipment_set_template_params)
    @equipment_set_template.contentid = EquipmentSetTemplate.next_id # присваивает следующий id
    if @equipment_set_template.save
      self.date
      redirect_to new_equipment_template_path(:va_code_int => @equipment_set_template.contentid, :est_name => @equipment_set_template.name), notice: 'Шаблок комплекта оборудования успешно создан.'
    else
      render :new
    end
  end

  # PATCH/PUT /equipment_set_templates/1
  def update
    if @equipment_set_template.update(equipment_set_template_params)
      self.date
      EquipmentSet.update_template(@equipment_set_template)

      redirect_to @equipment_set_template, notice: 'Шаблон комплекта оборудования успешно изменен.'
    else
      render :edit
    end
  end

  # DELETE /equipment_set_templates/1
  def destroy
    @equipment_set_template.destroy
    redirect_to equipment_set_templates_url, notice: 'Шаблон комплекта оборудования успешно удален.'
  end

  def create_by_template
    items = []
    EquipmentSetTemplate.find(params[:id]).create_by_template(items)
  end
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_equipment_set_template
      @equipment_set_template = EquipmentSetTemplate.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def equipment_set_template_params
      params.require(:equipment_set_template).permit(:name,:addvarchar_1, :addvarchar_2,:va_code_int, :addvarchar_3)
    end
end
