class CartridgeCategory < CatalogContent
  unloadable
  default_scope { where(catalogid: 71, prefix: 1)}
  scope :ordered, ->(){ order(:name)}

  def printers
    self.addvarchar_1
  end

  def category
    self.addinteger_2
  end

  # include Redmine::SafeAttributes
  # safe_attributes 'name', 'type'
  # attr_accessible 'name', 'type'
  # scope :ordered, ->(){order :name}
end
