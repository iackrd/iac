
class EquipmentTemplate < CatalogContent
  belongs_to :equipment_set_template, foreign_key: "va_code_int"
  has_many :equipment_unit, foreign_key: "unittypeid"
  belongs_to :equipment_category, foreign_key: "addinteger_1"
  around_save :patch_main
  around_destroy :destroy_main
  self.table_name = "CATALOGCONTENT"
  self.primary_key = "contentid"
  default_scope { where(catalogid: 79)}

private
  def patch_main
    if (self.addinteger_3 == 1)
      ActiveRecord::Base.transaction do
        yield
        ids = EquipmentTemplate.where({addinteger_3: '1', va_code_int: self.va_code_int})
                  .where("not (contentid = #{self.contentid})")
        if(ids.count>0);EquipmentTemplate.update(ids, addinteger_3: "0")end
      end
    else
      yield
    end
  end # служит для того чтоб  при добавлении нового оборудования главное оборудование было лишь одно

  def destroy_main
    if (self.addinteger_3 == 1)
      ActiveRecord::Base.transaction do
        yield
        ids = EquipmentTemplate.where({addinteger_3: '0', va_code_int: self.va_code_int})
        if(ids.count>0); ids.first.update( addinteger_3: "1") end
        end
    else
      yield
    end
  end # служит для того чтоб  при удалении главное оборудование первое попавшееся оборудования комплекта было главным
end
