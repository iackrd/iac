class OrganizationCartridge < ActiveRecord::Base
  unloadable
  belongs_to :cartridge_category
  scope :ordered, ->(){order :name}
end
