
class EquipmentSet < CiaDatabase
  has_many :equipment_unit, foreign_key: "complectid"
  has_many :equipment, through: :equipment_unit
  has_many :equipment_template, through: :equipment_unit
  belongs_to :owner, :class_name => 'Institution', foreign_key: "ownerid"
  belongs_to :location, :class_name => 'Institution', foreign_key: "setplaceid"
  has_one :institution_addresses, through: :location
  self.table_name = "EQUIPMENTCOMPLECT"
  self.primary_key = "id"

  def set_template
    if(self.equipment_template.first.nil?)
    else
      set_templte=self.equipment_template.first.equipment_set_template
    end
  end

  def self.create_repeated_invnum(row_csv,equipments,est)
    @equipment_set=EquipmentSet.new
    @equipment_set.id=EquipmentSet.last.id+1
    @equipment_set.getdate=est.addvarchar_1 # Дата получения
    @equipment_set.warrantyenddate=est.addvarchar_2 # дата завершения гарантии
    @equipment_set.cost=est.addvarchar_3 # стоимость комплекта
    if(est.va_code_int.nil?);@equipment_set.ownerid="56" else @equipment_set.ownerid =est.va_code_int end # владелец комплекта если в шаблоне не указан владелец то владелец ИАЦ
    @equipment_set.complectnum = row_csv["22"]
    @equipment_set.invnumber = row_csv["22"]
    @equipment_set.setplaceid = row_csv["23"]
    @equipment_set.bldgaddressid=@equipment_set.institution_addresses #адрес ОА
    est.equipment_template.each do |equipment_template|
      @equipment_set.name=@equipment_set.name.to_s+equipment_template.name+"; "
    end
    @equipment_set.save
    Equipment.repeated_invnum(row_csv,equipments,est,@equipment_set.id)
  end

  def self.create_csv(est,et, csv)
    csv.each do |set|
      @equipment_set=EquipmentSet.new
      @equipment_set.id=EquipmentSet.last.id+1 # следующий id комплекта
      @equipment_set.getdate=est.addvarchar_1 # Дата получения
      @equipment_set.warrantyenddate=est.addvarchar_2 # дата завершения гарантии
      @equipment_set.cost=est.addvarchar_3 # стоимость комплекта
      if(est.va_code_int.nil?);@equipment_set.ownerid="56" else @equipment_set.ownerid =est.va_code_int end # владелец комплекта если в шаблоне не указан владелец то владелец ИАЦ
      @equipment_set.complectnum = set["22"]
      @equipment_set.invnumber = set["22"]
      @equipment_set.setplaceid = set["23"]
      @equipment_set.bldgaddressid=@equipment_set.institution_addresses #адрес ОА
      et.each do |equipment_template|
        @equipment_set.name=@equipment_set.name.to_s+equipment_template.name+"; "
      end
      @equipment_set.save
      Equipment.create_csv(set, et, @equipment_set)
    end # Для каждого комплекта из csv
  end

  def self.update_template(params)
    if(params.catalogid==78)
      unit=EquipmentTemplate.where(va_code_int: params.contentid).first.equipment_unit
      unit.each do |set|
        @equipment_set=set.equipment_set
        @equipment_set.getdate= params.addvarchar_1
        @equipment_set.warrantyenddate = params.addvarchar_2
        @equipment_set.cost = params.addvarchar_3
        @equipment_set.ownerid = params.va_code_int
        @equipment_set.save
        @equipment_set.equipment.each do |equipmement|
          equipmement.getdate = @equipment_set.getdate
          equipmement.warrantyenddate = @equipment_set.warrantyenddate
          equipmement.ownerid = @equipment_set.ownerid
          equipmement.save
        end # сохранения изменений в еденице оборудования
      end
    end
  end # когда редактируется комплект шаблона

  def update_set
    self.equipment.each do |equipment|
      equipment.invnumber = self.invnumber
      equipment.setplaceid = self.setplaceid
      equipment.bldgaddressid= self.institution_addresses.id
      equipment.save
    end
  end # редактирование оборудования комплекта

  def create_set
    templete_set = EquipmentSetTemplate.where(contentid: self.complectnum).first
    self.getdate = templete_set[:addvarchar_1] #устанавливаю дату ввода в эксплуатацию
    self.warrantyenddate = templete_set[:addvarchar_2] #дата завершения гарантии
    self.cost = templete_set[:addvarchar_3] #стоимость комплекта
    if (templete_set[:va_code_int].nil?);self.ownerid="56" else self.ownerid = templete_set[:va_code_int]end #владелец комплекта если в шаблоне не указан владелец то владелец ИАЦ
    self.complectnum=invnumber #присваиваю номеру комплекта номер инвентарника
    self.bldgaddressid=institution_addresses.id #адрес ОА
    self.id=EquipmentSet.last.id+1# следующий id комплекта
    templete_set.equipment_template.each do |et|
      self.name=self.name.to_s+et.name+"; "
    end
    save
  end

end