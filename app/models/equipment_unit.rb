
class EquipmentUnit < CiaDatabase
  self.table_name = "EQUIPMENTCOMPLECTUNITS"
  self.primary_key = "equipmentid"
  belongs_to :equipment_template , foreign_key: "unittypeid"
  belongs_to :equipment_set , foreign_key: "complectid"
  belongs_to :equipment, foreign_key: "equipmentid"

  def self.new_unit(equipmentid, complectnum, template_equipment_id)
    @equipment_unit = EquipmentUnit.new
    @equipment_unit.equipmentid = equipmentid
    @equipment_unit.complectid =  complectnum
    @equipment_unit.unittypeid = template_equipment_id
    @equipment_unit.save
  end
end