
class EquipmentSetTemplate < CatalogContent
  has_many :equipment_template, foreign_key: "va_code_int", dependent: :delete_all
  belongs_to :catalog_content,  foreign_key: "contentid"
  self.table_name = "CATALOGCONTENT"
  self.primary_key = "contentid"
  default_scope { where(catalogid: 78)}
  before_save :editing_equipment

  def editing_equipment
    template=self.equipment_template
  end

  def create_csv(csv)
    est = self
    et = self.equipment_template.order('addinteger_3').reverse_order # чтоб главное оборудование было первым
    EquipmentSet.create_csv(est,et,csv)
  end

  def create_by_template(items)
    items.each do |item|
      inv = item[:inv]
      oa = item[:oa]
      item.remove(:inv)
      item.each_pair do |temlate_id, ser_num|
        equ
      end
    end
  end

end