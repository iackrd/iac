class CatalogContent < CiaDatabase
  has_many :equipment_set_template, foreign_key: "contentid"
  self.table_name = "CATALOGCONTENT"
  self.primary_key = "contentid"
  def self.last_id
    contentid=self.maximum(:contentid)
  end
  def self.next_id
    contentid=self.maximum(:contentid)+1
  end
end