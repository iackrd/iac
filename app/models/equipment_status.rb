class EquipmentStatus < CiaDatabase
  self.table_name = "CATALOGCONTENT"
  self.primary_key = "contentid"
  has_many :equipments, foreign_key: "statusid"
  default_scope { where(catalogid: 5)}
end